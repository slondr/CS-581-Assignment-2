#+TITLE: Assignment 2
#+AUTHOR: Eric S. Londres
#+OPTIONS: toc:nil
#+LATEX_HEADER: \usepackage[margin=0.6in]{geometry}
#+latex_compiler: xelatex
#+latex_header: \usepackage{libertine}
#+latex_header: \usepackage{unicode-math}


**** 
     + (a) :: [[./Pivotal.png]]
     + (b) :: In the above graph, Node A is pivotal to (B, D), Node B is pivotal to (A, C), Node C is pivotal to (B, E), Node D is pivotal to (A, E), and Node E is pivotal to (C, D)

**** 
     + (a) :: [[./Globally Pivotal.png]]
     + (b) :: Node E is pivotal for all paths between A, B, C, and D.
**** 
     Node B is pivotal for (G, *). Node A is pivotal for (B ∧ E ∧ F ∧ G, C ∧ D)
**** 
     STAN and UCSB are non-pivotal, as (SRI, UCLA) bypasses both of them.

**** 
     The following nodes are gatekeepers:
+ Node A is a gatekeeper for all paths to either C or D, except (C, D) itself.
+ Node B is a gatekeeper for all paths to G.
**** 
     + Node G is not a local gatekeeper as it is only connected to one node.
     + Node C and Node D are not local gatekeepers because they are each only connected to two nodes, and those nodes are connected to each other.

**** 
     There are no nodes in the DARPANET network structure which are gatekeepers so there is no single point of failure within the network structure which could lead to isolation of any node from the rest of the (functional) network.
**** 
     Triadic closure in a graph is the tendency of a connection to form between two nodes if they both already have a strong connection to some mutual third node. It plays a role in the formation of social networks as the concept maps very well to human interaction --- if Person A trusts Person B, and Person B trusts Person C, Person A will have more justification and reason to trust Person C, and likewise will have more opportunities to interact with them and form a relationship.
**** 
     Based on the strong/weak tie theory, I would expect the B-C edge to be a weak tie. If it were a strong tie, then there should be at least a week tie between F and A and between E and D. No such tie exists, so the triadic closure is not present, thus the B-C edge must be weak.
**** 
     Ties would immediately form between A-E and B-E to complete the triadic closure formed by A-C-E and B-C-E, respectively.
**** 
     [[./11.png]]
     With the exception of Node F, every node that Node C and Node E are connected to are also connected to Node A.
**** 
     No, this network is not balanced. Each node will have 29 positive edges and 60 negative edges, leading to significantly more negative than positive edges (which means the network is not balanced).
**** 
     | *Edge* | *Sign*   | *Triangles*   | *Balanced* | *Unbalanced* |
     |--------+----------+---------------+------------+--------------|
     | AB     | Positive | ABC, ABD, ABE | ABC, ABD   | ABE          |
     |--------+----------+---------------+------------+--------------|
     | AC     | Positive | ABC, ACD, ACE | ABC, ACE   | ADC          |
     |--------+----------+---------------+------------+--------------|
     | AD     | Negative | ABD, ACD, ACE | ABD        | ACD, ADE     |
     |--------+----------+---------------+------------+--------------|
     | AE     | Negative | ABE, ACE, ADE | ACE        | ABE, ADE     |
     |--------+----------+---------------+------------+--------------|
     | BC     | Negative | ABC, BCD, BCE | ABC        | BCD, BCE     |
     |--------+----------+---------------+------------+--------------|
     | BD     | Positive | ABD, CBD, BDE | ABD, BDE   | CBD          |
     |--------+----------+---------------+------------+--------------|
     | BE     | Negative | ABE, BCE, BDE | BDE        | ABE, BCE     |
     |--------+----------+---------------+------------+--------------|
     | CD     | Negative | ACD, BCD, CDE | CDE        | ACD, BCD     |
     |--------+----------+---------------+------------+--------------|
     | CE     | Positive | ACE, BCE, CDE | ACE, CDE   | BCE          |
     |--------+----------+---------------+------------+--------------|
     | DE     | Positive | ADE, BDE, CDE | BDE, CDE   | ADE          |

